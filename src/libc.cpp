#include <iostream>
#include <libc/libc.h>

void libc::print(std::string_view str)
{
    std::cout.write(str.data(), str.size());
}

void libc::flush()
{
    std::cout.flush();
}

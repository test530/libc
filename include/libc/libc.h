#pragma once

#include <string_view>

namespace libc
{
    void print(std::string_view str);
    void flush();
}
